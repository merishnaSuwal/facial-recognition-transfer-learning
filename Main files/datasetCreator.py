import numpy as np
import cv2
import os

faceDetect = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# Get the name and id
id = input("Enter user id : ")
name = input("Enter username:")
name =  "_".join(name.lower().split(" "))
# Making the directories
faceDataSet = "dataSet/"+ name
os.makedirs(faceDataSet)
sampleNum = 0

# CAM initialized
cam = cv2.VideoCapture(0)
while (True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceDetect.detectMultiScale(gray,
                                        scaleFactor=2.0,
                                        minNeighbors=5
                                        )
    for (x, y, w, h) in faces:
        sampleNum = sampleNum + 1
        cv2.imwrite(faceDataSet + "/User." + str(id) + "." + str(sampleNum) + ".jpg", gray[y:y + h, x:x + w])
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

    cv2.imshow('face', img)
    cv2.waitKey(1)
    if sampleNum == 100:
        break
cam.release()
cv2.destroyAllWindows()


