import numpy as np
import cv2
import tensorflow as tf, sys
id=1
faceDetect = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
fontFace = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
fontColor = (255, 0, 0)
# for record not found case
fontColor1 = (0, 0, 255)
# Loads label file, strips off carriage return
label_lines = [line.rstrip() for line
               in tf.gfile.GFile("./tmp/output_labels.txt")]

# Unpersists graph from file
with tf.gfile.FastGFile("./tmp/output_graph.pb", 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

# now all we need to do is give tensorflow the image data
cam = cv2.VideoCapture(0)

# # IF USING GPU, ADD THESE
# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=1)
with tf.Session() as sess:
    # Feed the image_data as input to the graph and get first prediction
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

    while (True):
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = faceDetect.detectMultiScale(gray,
                                            scaleFactor=1.3,
                                            minNeighbors=5
                                            )
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            tfImage = np.array(img)[:, :, 0:3]
            predictions = sess.run(softmax_tensor,
                                   {'DecodeJpeg:0': tfImage})
            # Sort to show labels of first prediction in order of confidence
            top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
            s = 0
            for node_id in top_k:
                human_string = label_lines[node_id]
                score = predictions[0][node_id]
                if (score > s):
                    s = score * 100
                    h_name = human_string
            cv2.putText(img, str(h_name), (x, y + h + 30), fontFace, fontScale, fontColor)
            cv2.putText(img, "Acc: {}".format(str(round(s, 2))), (x, y + h + 60), fontFace, fontScale, fontColor)
        cv2.imshow('face', img);
        if ord('q') == cv2.waitKey(1):
            break
    cam.release()
    cv2.destroyAllWindows()